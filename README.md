# DEMO API TESTS

### TECH: JAVA SPRING {MAVEN} CUCUMBER JVM

-----

#### Requirements
* JAVA JDK 8+
* MAVEN 

#### Execution: 
##### Linux / MAC 

``` 
./run.sh
```
--- 
 
##### Allure Reporte
```
allure serve target/allure-results
``` 
 
 
##### Windows
```
mvn -U -DtestConfig=live clean test
```

##### Saved Allure Report
```
http://allure.suirtech.com:8601
```

