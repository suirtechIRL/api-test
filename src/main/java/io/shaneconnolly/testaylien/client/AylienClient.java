package io.shaneconnolly.testaylien.client;

import io.shaneconnolly.testaylien.model.responses.clusters.ClustersResponse;
import io.shaneconnolly.testaylien.model.responses.stories.StoriesResponse;
import io.shaneconnolly.testaylien.model.responses.errors.ErrorResponse;
import org.springframework.http.ResponseEntity;

public interface AylienClient {

    ResponseEntity<StoriesResponse> getStories();

    ResponseEntity<StoriesResponse> getStory(int id);

    ResponseEntity<ClustersResponse> getClusters();

    ResponseEntity<ClustersResponse> getClusters(int storyCountMin, int storyCountMax);

    ErrorResponse getStoriesNoAuth();

}
