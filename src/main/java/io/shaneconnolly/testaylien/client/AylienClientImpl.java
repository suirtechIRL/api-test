package io.shaneconnolly.testaylien.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.shaneconnolly.testaylien.model.responses.clusters.ClustersResponse;
import io.shaneconnolly.testaylien.model.responses.errors.ErrorResponse;
import io.shaneconnolly.testaylien.model.responses.stories.StoriesResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;


@Component
public class AylienClientImpl implements AylienClient {

    @Value("${api.url}")
    private String apiUrl;

    private final RestTemplate restTemplate;
    private final HttpHeaders headers;

    private static final String STORIES_ENDPOINT = "/news/stories";
    private static final String CLUSTERS_ENDPOINT = "/news/clusters";

    @Autowired
    public AylienClientImpl(RestTemplate restTemplate, HttpHeaders httpHeaders) {
        this.restTemplate = restTemplate;
        this.headers = httpHeaders;
    }

    @Override
    public ResponseEntity<StoriesResponse> getStories() {
        return restTemplate.exchange(apiUrl + STORIES_ENDPOINT, HttpMethod.GET,
                new HttpEntity<>(null, headers), StoriesResponse.class);
    }

    @Override
    public ResponseEntity<StoriesResponse> getStory(int id) {
        String queryString = String.format("?id=%s", id);
        return restTemplate.exchange(apiUrl + STORIES_ENDPOINT + queryString, HttpMethod.GET,
                new HttpEntity<>(null, headers), StoriesResponse.class);
    }

    @Override
    public ResponseEntity<ClustersResponse> getClusters() {
        return null;
    }

    @Override
    public ResponseEntity<ClustersResponse> getClusters(int storyCountMin, int storyCountMax) {

        UriComponentsBuilder uriBuilder = geDefaultURIBuilder()
                .queryParam("story_count.min", storyCountMin)
                .queryParam("story_count.max", storyCountMax);

        return restTemplate.exchange(
                uriBuilder.toUriString(),
                HttpMethod.GET,
                new HttpEntity<>(null, headers),
                ClustersResponse.class);
    }


    @Override
    public ErrorResponse getStoriesNoAuth() {
        try {
            return restTemplate.exchange(apiUrl + STORIES_ENDPOINT, HttpMethod.GET,
                    new HttpEntity<>(null, null), ErrorResponse.class).getBody();
        } catch (HttpClientErrorException ex) {
            if (ex.getStatusCode() == HttpStatus.UNAUTHORIZED) {

                String responseString = ex.getResponseBodyAsString();

                ObjectMapper mapper = new ObjectMapper();

                try {
                    return mapper.readValue(responseString,
                            ErrorResponse.class);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }
    }

    private UriComponentsBuilder geDefaultURIBuilder() {
        return UriComponentsBuilder.fromHttpUrl(apiUrl + CLUSTERS_ENDPOINT)
                .queryParam("language", "en");
    }
}
