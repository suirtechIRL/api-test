package io.shaneconnolly.testaylien.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.converter.ResourceHttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;

@Configuration
@PropertySource("classpath:/${testConfig}.properties")
public class ClientConfig {

    @Value("${app.id}")
    private String appID;

    @Value("${app.key}")
    private String appKey;

    @Bean
    public RestTemplate restTemplate() {
        RestTemplate rest = new RestTemplate();

        rest.setMessageConverters(Arrays.asList(
                new ResourceHttpMessageConverter(),
                new MappingJackson2HttpMessageConverter(),
                new StringHttpMessageConverter()));

        return rest;
    }


    @Bean
    public HttpHeaders getHeaders(){
        HttpHeaders headers = new HttpHeaders();
        headers.set("X-AYLIEN-NewsAPI-Application-ID", appID);
        headers.set("X-AYLIEN-NewsAPI-Application-Key", appKey);

        return headers;
    }
}
