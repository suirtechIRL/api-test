package io.shaneconnolly.testaylien.business.actions;

import io.shaneconnolly.testaylien.model.responses.clusters.Cluster;
import io.shaneconnolly.testaylien.model.responses.stories.Story;
import io.shaneconnolly.testaylien.model.responses.stories.UpdatedCount;
import io.shaneconnolly.testaylien.support.CustomAssertions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;


@Component
public class VerifyResponseActions {

    @Autowired
    private CustomAssertions customAssertions;

    private static final int HOURS_TO_FAIL_IF_SCOUNT_NOT_UPDATED = 48;

    public void verifySocialSharesUpdating(Story savedStory, Story updatedStory) {
        // done for facebook only
        customAssertions.assertGreaterThanOrEqualTo(savedStory.getSocialSharesCount().getFacebook().size(),
                updatedStory.getSocialSharesCount().getFacebook().size());

        List<UpdatedCount> facebookSCs = updatedStory.getSocialSharesCount().getFacebook();

        for (UpdatedCount updatedCount : facebookSCs) {
            customAssertions.assertGreaterThanOrEqualTo(HOURS_TO_FAIL_IF_SCOUNT_NOT_UPDATED,
                    getHrsSinceUpdated(updatedCount.getFetchedAt()));
        }

    }


    public void verifyStoryId(long id, Story story) {
        customAssertions.assertEquals(id, story.getId());
    }

    public void verifyMinMaxClusterStorySize(List<Cluster> clusters, int minStoryCount, int maxStoryCount) {
        for (Cluster cluster : clusters) {
            customAssertions.assertGreaterThanOrEqualTo(cluster.getStoryCount(), minStoryCount);
            customAssertions.assertLessThanOrEqualTo(cluster.getStoryCount(), maxStoryCount);
        }
    }


    private int getHrsSinceUpdated(Date updatedDate) {
        Date now = new Date();
        long diff = now.getTime() - updatedDate.getTime();
        return (int) (diff / (60 * 60 * 1000));
    }

}
