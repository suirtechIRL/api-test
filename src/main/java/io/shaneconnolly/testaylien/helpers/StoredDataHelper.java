package io.shaneconnolly.testaylien.helpers;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.shaneconnolly.testaylien.model.responses.stories.Story;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Component
public class StoredDataHelper {


    public void saveStory(Story story) throws IOException {
        String storyString = new ObjectMapper().writeValueAsString(story);
        Path path = Paths.get(String.format("./data_from_test_store/saved_story_%s.json", story.getId()));
        Files.write(path, storyString.getBytes());
    }

    public Story getSavedStory(int id) throws IOException {
        String jsonStory = new String(Files.readAllBytes(Paths.get(String.format("./data_from_test_store/saved_story_%d.json", id))),
                StandardCharsets.UTF_8);

        return new ObjectMapper().readValue(jsonStory, Story.class);
    }

}
