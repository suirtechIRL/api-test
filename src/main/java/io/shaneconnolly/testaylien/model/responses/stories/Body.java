
package io.shaneconnolly.testaylien.model.responses.stories;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "text",
    "score",
    "types",
    "links",
    "indices"
})
public class Body {

    @JsonProperty("text")
    private String text;
    @JsonProperty("score")
    private double score;
    @JsonProperty("types")
    private List<String> types = null;
    @JsonProperty("links")
    private Links links;
    @JsonProperty("indices")
    private List<List<Long>> indices = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public Body() {
    }

    /**
     * 
     * @param score
     * @param types
     * @param indices
     * @param links
     * @param text
     */
    public Body(String text, double score, List<String> types, Links links, List<List<Long>> indices) {
        super();
        this.text = text;
        this.score = score;
        this.types = types;
        this.links = links;
        this.indices = indices;
    }

    @JsonProperty("text")
    public String getText() {
        return text;
    }

    @JsonProperty("text")
    public void setText(String text) {
        this.text = text;
    }

    public Body withText(String text) {
        this.text = text;
        return this;
    }

    @JsonProperty("score")
    public double getScore() {
        return score;
    }

    @JsonProperty("score")
    public void setScore(double score) {
        this.score = score;
    }

    public Body withScore(double score) {
        this.score = score;
        return this;
    }

    @JsonProperty("types")
    public List<String> getTypes() {
        return types;
    }

    @JsonProperty("types")
    public void setTypes(List<String> types) {
        this.types = types;
    }

    public Body withTypes(List<String> types) {
        this.types = types;
        return this;
    }

    @JsonProperty("links")
    public Links getLinks() {
        return links;
    }

    @JsonProperty("links")
    public void setLinks(Links links) {
        this.links = links;
    }

    public Body withLinks(Links links) {
        this.links = links;
        return this;
    }

    @JsonProperty("indices")
    public List<List<Long>> getIndices() {
        return indices;
    }

    @JsonProperty("indices")
    public void setIndices(List<List<Long>> indices) {
        this.indices = indices;
    }

    public Body withIndices(List<List<Long>> indices) {
        this.indices = indices;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public Body withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("text", text).append("score", score).append("types", types).append("links", links).append("indices", indices).append("additionalProperties", additionalProperties).toString();
    }

}
