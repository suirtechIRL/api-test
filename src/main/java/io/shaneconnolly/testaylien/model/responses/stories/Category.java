
package io.shaneconnolly.testaylien.model.responses.stories;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "taxonomy",
    "level",
    "score",
    "confident",
    "links"
})
public class Category {

    @JsonProperty("id")
    private String id;
    @JsonProperty("taxonomy")
    private String taxonomy;
    @JsonProperty("level")
    private long level;
    @JsonProperty("score")
    private double score;
    @JsonProperty("confident")
    private boolean confident;
    @JsonProperty("links")
    private Links_ links;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public Category() {
    }

    /**
     * 
     * @param score
     * @param level
     * @param confident
     * @param links
     * @param id
     * @param taxonomy
     */
    public Category(String id, String taxonomy, long level, double score, boolean confident, Links_ links) {
        super();
        this.id = id;
        this.taxonomy = taxonomy;
        this.level = level;
        this.score = score;
        this.confident = confident;
        this.links = links;
    }

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    public Category withId(String id) {
        this.id = id;
        return this;
    }

    @JsonProperty("taxonomy")
    public String getTaxonomy() {
        return taxonomy;
    }

    @JsonProperty("taxonomy")
    public void setTaxonomy(String taxonomy) {
        this.taxonomy = taxonomy;
    }

    public Category withTaxonomy(String taxonomy) {
        this.taxonomy = taxonomy;
        return this;
    }

    @JsonProperty("level")
    public long getLevel() {
        return level;
    }

    @JsonProperty("level")
    public void setLevel(long level) {
        this.level = level;
    }

    public Category withLevel(long level) {
        this.level = level;
        return this;
    }

    @JsonProperty("score")
    public double getScore() {
        return score;
    }

    @JsonProperty("score")
    public void setScore(double score) {
        this.score = score;
    }

    public Category withScore(double score) {
        this.score = score;
        return this;
    }

    @JsonProperty("confident")
    public boolean isConfident() {
        return confident;
    }

    @JsonProperty("confident")
    public void setConfident(boolean confident) {
        this.confident = confident;
    }

    public Category withConfident(boolean confident) {
        this.confident = confident;
        return this;
    }

    @JsonProperty("links")
    public Links_ getLinks() {
        return links;
    }

    @JsonProperty("links")
    public void setLinks(Links_ links) {
        this.links = links;
    }

    public Category withLinks(Links_ links) {
        this.links = links;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public Category withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("taxonomy", taxonomy).append("level", level).append("score", score).append("confident", confident).append("links", links).append("additionalProperties", additionalProperties).toString();
    }

}
