
package io.shaneconnolly.testaylien.model.responses.stories;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "title",
    "body"
})
public class Sentiment {

    @JsonProperty("title")
    private Title title;
    @JsonProperty("body")
    private Body_ body;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public Sentiment() {
    }

    /**
     * 
     * @param title
     * @param body
     */
    public Sentiment(Title title, Body_ body) {
        super();
        this.title = title;
        this.body = body;
    }

    @JsonProperty("title")
    public Title getTitle() {
        return title;
    }

    @JsonProperty("title")
    public void setTitle(Title title) {
        this.title = title;
    }

    public Sentiment withTitle(Title title) {
        this.title = title;
        return this;
    }

    @JsonProperty("body")
    public Body_ getBody() {
        return body;
    }

    @JsonProperty("body")
    public void setBody(Body_ body) {
        this.body = body;
    }

    public Sentiment withBody(Body_ body) {
        this.body = body;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public Sentiment withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("title", title).append("body", body).append("additionalProperties", additionalProperties).toString();
    }

}
