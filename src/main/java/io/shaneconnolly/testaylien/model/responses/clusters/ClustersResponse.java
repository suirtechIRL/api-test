
package io.shaneconnolly.testaylien.model.responses.clusters;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "clusters",
    "next_page_cursor",
    "cluster_count"
})
public class ClustersResponse {

    @JsonProperty("clusters")
    private List<Cluster> clusters = null;
    @JsonProperty("next_page_cursor")
    private String nextPageCursor;
    @JsonProperty("cluster_count")
    private Integer clusterCount;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public ClustersResponse() {
    }

    /**
     * 
     * @param clusterCount
     * @param nextPageCursor
     * @param clusters
     */
    public ClustersResponse(List<Cluster> clusters, String nextPageCursor, Integer clusterCount) {
        super();
        this.clusters = clusters;
        this.nextPageCursor = nextPageCursor;
        this.clusterCount = clusterCount;
    }

    @JsonProperty("clusters")
    public List<Cluster> getClusters() {
        return clusters;
    }

    @JsonProperty("clusters")
    public void setClusters(List<Cluster> clusters) {
        this.clusters = clusters;
    }

    public ClustersResponse withClusters(List<Cluster> clusters) {
        this.clusters = clusters;
        return this;
    }

    @JsonProperty("next_page_cursor")
    public String getNextPageCursor() {
        return nextPageCursor;
    }

    @JsonProperty("next_page_cursor")
    public void setNextPageCursor(String nextPageCursor) {
        this.nextPageCursor = nextPageCursor;
    }

    public ClustersResponse withNextPageCursor(String nextPageCursor) {
        this.nextPageCursor = nextPageCursor;
        return this;
    }

    @JsonProperty("cluster_count")
    public Integer getClusterCount() {
        return clusterCount;
    }

    @JsonProperty("cluster_count")
    public void setClusterCount(Integer clusterCount) {
        this.clusterCount = clusterCount;
    }

    public ClustersResponse withClusterCount(Integer clusterCount) {
        this.clusterCount = clusterCount;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public ClustersResponse withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("clusters", clusters).append("nextPageCursor", nextPageCursor).append("clusterCount", clusterCount).append("additionalProperties", additionalProperties).toString();
    }

}
