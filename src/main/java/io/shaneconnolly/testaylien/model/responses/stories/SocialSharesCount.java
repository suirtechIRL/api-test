
package io.shaneconnolly.testaylien.model.responses.stories;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "facebook",
    "google_plus",
    "linkedin",
    "reddit"
})
public class SocialSharesCount {

    @JsonProperty("facebook")
    private List<UpdatedCount> facebook = null;
    @JsonProperty("google_plus")
    private List<Object> googlePlus = null;
    @JsonProperty("linkedin")
    private List<Object> linkedin = null;
    @JsonProperty("reddit")
    private List<Object> reddit = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public SocialSharesCount() {
    }

    /**
     * 
     * @param facebook
     * @param reddit
     * @param linkedin
     * @param googlePlus
     */
    public SocialSharesCount(List<UpdatedCount> facebook, List<Object> googlePlus, List<Object> linkedin, List<Object> reddit) {
        super();
        this.facebook = facebook;
        this.googlePlus = googlePlus;
        this.linkedin = linkedin;
        this.reddit = reddit;
    }

    @JsonProperty("facebook")
    public List<UpdatedCount> getFacebook() {
        return facebook;
    }

    @JsonProperty("facebook")
    public void setFacebook(List<UpdatedCount> facebook) {
        this.facebook = facebook;
    }

    public SocialSharesCount withFacebook(List<UpdatedCount> facebook) {
        this.facebook = facebook;
        return this;
    }

    @JsonProperty("google_plus")
    public List<Object> getGooglePlus() {
        return googlePlus;
    }

    @JsonProperty("google_plus")
    public void setGooglePlus(List<Object> googlePlus) {
        this.googlePlus = googlePlus;
    }

    public SocialSharesCount withGooglePlus(List<Object> googlePlus) {
        this.googlePlus = googlePlus;
        return this;
    }

    @JsonProperty("linkedin")
    public List<Object> getLinkedin() {
        return linkedin;
    }

    @JsonProperty("linkedin")
    public void setLinkedin(List<Object> linkedin) {
        this.linkedin = linkedin;
    }

    public SocialSharesCount withLinkedin(List<Object> linkedin) {
        this.linkedin = linkedin;
        return this;
    }

    @JsonProperty("reddit")
    public List<Object> getReddit() {
        return reddit;
    }

    @JsonProperty("reddit")
    public void setReddit(List<Object> reddit) {
        this.reddit = reddit;
    }

    public SocialSharesCount withReddit(List<Object> reddit) {
        this.reddit = reddit;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public SocialSharesCount withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("facebook", facebook).append("googlePlus", googlePlus).append("linkedin", linkedin).append("reddit", reddit).append("additionalProperties", additionalProperties).toString();
    }

}
