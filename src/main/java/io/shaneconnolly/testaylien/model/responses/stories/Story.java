
package io.shaneconnolly.testaylien.model.responses.stories;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "title",
    "body",
    "summary",
    "source",
    "author",
    "entities",
    "keywords",
    "hashtags",
    "characters_count",
    "words_count",
    "sentences_count",
    "paragraphs_count",
    "categories",
    "social_shares_count",
    "media",
    "sentiment",
    "language",
    "published_at",
    "links",
    "clusters",
    "translations"
})
public class Story {

    @JsonProperty("id")
    private long id;
    @JsonProperty("title")
    private String title;
    @JsonProperty("body")
    private String body;
    @JsonProperty("summary")
    private Summary summary;
    @JsonProperty("source")
    private Source source;
    @JsonProperty("author")
    private Author author;
    @JsonProperty("entities")
    private Entities entities;
    @JsonProperty("keywords")
    private List<String> keywords = null;
    @JsonProperty("hashtags")
    private List<String> hashtags = null;
    @JsonProperty("characters_count")
    private long charactersCount;
    @JsonProperty("words_count")
    private long wordsCount;
    @JsonProperty("sentences_count")
    private long sentencesCount;
    @JsonProperty("paragraphs_count")
    private long paragraphsCount;
    @JsonProperty("categories")
    private List<Category> categories = null;
    @JsonProperty("social_shares_count")
    private SocialSharesCount socialSharesCount;
    @JsonProperty("media")
    private List<Medium> media = null;
    @JsonProperty("sentiment")
    private Sentiment sentiment;
    @JsonProperty("language")
    private String language;
    @JsonProperty("published_at")
    private String publishedAt;
    @JsonProperty("links")
    private Links__ links;
    @JsonProperty("clusters")
    private List<Long> clusters = null;
    @JsonProperty("translations")
    private Translations translations;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public Story() {
    }

    /**
     * 
     * @param summary
     * @param wordsCount
     * @param charactersCount
     * @param sentiment
     * @param keywords
     * @param hashtags
     * @param publishedAt
     * @param author
     * @param sentencesCount
     * @param language
     * @param source
     * @param media
     * @param title
     * @param body
     * @param paragraphsCount
     * @param socialSharesCount
     * @param entities
     * @param translations
     * @param links
     * @param id
     * @param categories
     * @param clusters
     */
    public Story(long id, String title, String body, Summary summary, Source source, Author author, Entities entities, List<String> keywords, List<String> hashtags, long charactersCount, long wordsCount, long sentencesCount, long paragraphsCount, List<Category> categories, SocialSharesCount socialSharesCount, List<Medium> media, Sentiment sentiment, String language, String publishedAt, Links__ links, List<Long> clusters, Translations translations) {
        super();
        this.id = id;
        this.title = title;
        this.body = body;
        this.summary = summary;
        this.source = source;
        this.author = author;
        this.entities = entities;
        this.keywords = keywords;
        this.hashtags = hashtags;
        this.charactersCount = charactersCount;
        this.wordsCount = wordsCount;
        this.sentencesCount = sentencesCount;
        this.paragraphsCount = paragraphsCount;
        this.categories = categories;
        this.socialSharesCount = socialSharesCount;
        this.media = media;
        this.sentiment = sentiment;
        this.language = language;
        this.publishedAt = publishedAt;
        this.links = links;
        this.clusters = clusters;
        this.translations = translations;
    }

    @JsonProperty("id")
    public long getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(long id) {
        this.id = id;
    }

    public Story withId(long id) {
        this.id = id;
        return this;
    }

    @JsonProperty("title")
    public String getTitle() {
        return title;
    }

    @JsonProperty("title")
    public void setTitle(String title) {
        this.title = title;
    }

    public Story withTitle(String title) {
        this.title = title;
        return this;
    }

    @JsonProperty("body")
    public String getBody() {
        return body;
    }

    @JsonProperty("body")
    public void setBody(String body) {
        this.body = body;
    }

    public Story withBody(String body) {
        this.body = body;
        return this;
    }

    @JsonProperty("summary")
    public Summary getSummary() {
        return summary;
    }

    @JsonProperty("summary")
    public void setSummary(Summary summary) {
        this.summary = summary;
    }

    public Story withSummary(Summary summary) {
        this.summary = summary;
        return this;
    }

    @JsonProperty("source")
    public Source getSource() {
        return source;
    }

    @JsonProperty("source")
    public void setSource(Source source) {
        this.source = source;
    }

    public Story withSource(Source source) {
        this.source = source;
        return this;
    }

    @JsonProperty("author")
    public Author getAuthor() {
        return author;
    }

    @JsonProperty("author")
    public void setAuthor(Author author) {
        this.author = author;
    }

    public Story withAuthor(Author author) {
        this.author = author;
        return this;
    }

    @JsonProperty("entities")
    public Entities getEntities() {
        return entities;
    }

    @JsonProperty("entities")
    public void setEntities(Entities entities) {
        this.entities = entities;
    }

    public Story withEntities(Entities entities) {
        this.entities = entities;
        return this;
    }

    @JsonProperty("keywords")
    public List<String> getKeywords() {
        return keywords;
    }

    @JsonProperty("keywords")
    public void setKeywords(List<String> keywords) {
        this.keywords = keywords;
    }

    public Story withKeywords(List<String> keywords) {
        this.keywords = keywords;
        return this;
    }

    @JsonProperty("hashtags")
    public List<String> getHashtags() {
        return hashtags;
    }

    @JsonProperty("hashtags")
    public void setHashtags(List<String> hashtags) {
        this.hashtags = hashtags;
    }

    public Story withHashtags(List<String> hashtags) {
        this.hashtags = hashtags;
        return this;
    }

    @JsonProperty("characters_count")
    public long getCharactersCount() {
        return charactersCount;
    }

    @JsonProperty("characters_count")
    public void setCharactersCount(long charactersCount) {
        this.charactersCount = charactersCount;
    }

    public Story withCharactersCount(long charactersCount) {
        this.charactersCount = charactersCount;
        return this;
    }

    @JsonProperty("words_count")
    public long getWordsCount() {
        return wordsCount;
    }

    @JsonProperty("words_count")
    public void setWordsCount(long wordsCount) {
        this.wordsCount = wordsCount;
    }

    public Story withWordsCount(long wordsCount) {
        this.wordsCount = wordsCount;
        return this;
    }

    @JsonProperty("sentences_count")
    public long getSentencesCount() {
        return sentencesCount;
    }

    @JsonProperty("sentences_count")
    public void setSentencesCount(long sentencesCount) {
        this.sentencesCount = sentencesCount;
    }

    public Story withSentencesCount(long sentencesCount) {
        this.sentencesCount = sentencesCount;
        return this;
    }

    @JsonProperty("paragraphs_count")
    public long getParagraphsCount() {
        return paragraphsCount;
    }

    @JsonProperty("paragraphs_count")
    public void setParagraphsCount(long paragraphsCount) {
        this.paragraphsCount = paragraphsCount;
    }

    public Story withParagraphsCount(long paragraphsCount) {
        this.paragraphsCount = paragraphsCount;
        return this;
    }

    @JsonProperty("categories")
    public List<Category> getCategories() {
        return categories;
    }

    @JsonProperty("categories")
    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public Story withCategories(List<Category> categories) {
        this.categories = categories;
        return this;
    }

    @JsonProperty("social_shares_count")
    public SocialSharesCount getSocialSharesCount() {
        return socialSharesCount;
    }

    @JsonProperty("social_shares_count")
    public void setSocialSharesCount(SocialSharesCount socialSharesCount) {
        this.socialSharesCount = socialSharesCount;
    }

    public Story withSocialSharesCount(SocialSharesCount socialSharesCount) {
        this.socialSharesCount = socialSharesCount;
        return this;
    }

    @JsonProperty("media")
    public List<Medium> getMedia() {
        return media;
    }

    @JsonProperty("media")
    public void setMedia(List<Medium> media) {
        this.media = media;
    }

    public Story withMedia(List<Medium> media) {
        this.media = media;
        return this;
    }

    @JsonProperty("sentiment")
    public Sentiment getSentiment() {
        return sentiment;
    }

    @JsonProperty("sentiment")
    public void setSentiment(Sentiment sentiment) {
        this.sentiment = sentiment;
    }

    public Story withSentiment(Sentiment sentiment) {
        this.sentiment = sentiment;
        return this;
    }

    @JsonProperty("language")
    public String getLanguage() {
        return language;
    }

    @JsonProperty("language")
    public void setLanguage(String language) {
        this.language = language;
    }

    public Story withLanguage(String language) {
        this.language = language;
        return this;
    }

    @JsonProperty("published_at")
    public String getPublishedAt() {
        return publishedAt;
    }

    @JsonProperty("published_at")
    public void setPublishedAt(String publishedAt) {
        this.publishedAt = publishedAt;
    }

    public Story withPublishedAt(String publishedAt) {
        this.publishedAt = publishedAt;
        return this;
    }

    @JsonProperty("links")
    public Links__ getLinks() {
        return links;
    }

    @JsonProperty("links")
    public void setLinks(Links__ links) {
        this.links = links;
    }

    public Story withLinks(Links__ links) {
        this.links = links;
        return this;
    }

    @JsonProperty("clusters")
    public List<Long> getClusters() {
        return clusters;
    }

    @JsonProperty("clusters")
    public void setClusters(List<Long> clusters) {
        this.clusters = clusters;
    }

    public Story withClusters(List<Long> clusters) {
        this.clusters = clusters;
        return this;
    }

    @JsonProperty("translations")
    public Translations getTranslations() {
        return translations;
    }

    @JsonProperty("translations")
    public void setTranslations(Translations translations) {
        this.translations = translations;
    }

    public Story withTranslations(Translations translations) {
        this.translations = translations;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public Story withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("title", title).append("body", body).append("summary", summary).append("source", source).append("author", author).append("entities", entities).append("keywords", keywords).append("hashtags", hashtags).append("charactersCount", charactersCount).append("wordsCount", wordsCount).append("sentencesCount", sentencesCount).append("paragraphsCount", paragraphsCount).append("categories", categories).append("socialSharesCount", socialSharesCount).append("media", media).append("sentiment", sentiment).append("language", language).append("publishedAt", publishedAt).append("links", links).append("clusters", clusters).append("translations", translations).append("additionalProperties", additionalProperties).toString();
    }

}
