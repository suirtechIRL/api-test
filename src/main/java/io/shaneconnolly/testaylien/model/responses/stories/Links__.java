
package io.shaneconnolly.testaylien.model.responses.stories;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "permalink",
    "related_stories",
    "coverages",
    "clusters"
})
public class Links__ {

    @JsonProperty("permalink")
    private String permalink;
    @JsonProperty("related_stories")
    private String relatedStories;
    @JsonProperty("coverages")
    private String coverages;
    @JsonProperty("clusters")
    private String clusters;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public Links__() {
    }

    /**
     * 
     * @param permalink
     * @param relatedStories
     * @param coverages
     * @param clusters
     */
    public Links__(String permalink, String relatedStories, String coverages, String clusters) {
        super();
        this.permalink = permalink;
        this.relatedStories = relatedStories;
        this.coverages = coverages;
        this.clusters = clusters;
    }

    @JsonProperty("permalink")
    public String getPermalink() {
        return permalink;
    }

    @JsonProperty("permalink")
    public void setPermalink(String permalink) {
        this.permalink = permalink;
    }

    public Links__ withPermalink(String permalink) {
        this.permalink = permalink;
        return this;
    }

    @JsonProperty("related_stories")
    public String getRelatedStories() {
        return relatedStories;
    }

    @JsonProperty("related_stories")
    public void setRelatedStories(String relatedStories) {
        this.relatedStories = relatedStories;
    }

    public Links__ withRelatedStories(String relatedStories) {
        this.relatedStories = relatedStories;
        return this;
    }

    @JsonProperty("coverages")
    public String getCoverages() {
        return coverages;
    }

    @JsonProperty("coverages")
    public void setCoverages(String coverages) {
        this.coverages = coverages;
    }

    public Links__ withCoverages(String coverages) {
        this.coverages = coverages;
        return this;
    }

    @JsonProperty("clusters")
    public String getClusters() {
        return clusters;
    }

    @JsonProperty("clusters")
    public void setClusters(String clusters) {
        this.clusters = clusters;
    }

    public Links__ withClusters(String clusters) {
        this.clusters = clusters;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public Links__ withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("permalink", permalink).append("relatedStories", relatedStories).append("coverages", coverages).append("clusters", clusters).append("additionalProperties", additionalProperties).toString();
    }

}
