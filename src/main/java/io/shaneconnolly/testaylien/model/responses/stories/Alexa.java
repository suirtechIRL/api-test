
package io.shaneconnolly.testaylien.model.responses.stories;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "rank",
    "fetched_at",
    "country"
})
public class Alexa {

    @JsonProperty("rank")
    private long rank;
    @JsonProperty("fetched_at")
    private String fetchedAt;
    @JsonProperty("country")
    private String country;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public Alexa() {
    }

    /**
     * 
     * @param country
     * @param rank
     * @param fetchedAt
     */
    public Alexa(long rank, String fetchedAt, String country) {
        super();
        this.rank = rank;
        this.fetchedAt = fetchedAt;
        this.country = country;
    }

    @JsonProperty("rank")
    public long getRank() {
        return rank;
    }

    @JsonProperty("rank")
    public void setRank(long rank) {
        this.rank = rank;
    }

    public Alexa withRank(long rank) {
        this.rank = rank;
        return this;
    }

    @JsonProperty("fetched_at")
    public String getFetchedAt() {
        return fetchedAt;
    }

    @JsonProperty("fetched_at")
    public void setFetchedAt(String fetchedAt) {
        this.fetchedAt = fetchedAt;
    }

    public Alexa withFetchedAt(String fetchedAt) {
        this.fetchedAt = fetchedAt;
        return this;
    }

    @JsonProperty("country")
    public String getCountry() {
        return country;
    }

    @JsonProperty("country")
    public void setCountry(String country) {
        this.country = country;
    }

    public Alexa withCountry(String country) {
        this.country = country;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public Alexa withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("rank", rank).append("fetchedAt", fetchedAt).append("country", country).append("additionalProperties", additionalProperties).toString();
    }

}
