
package io.shaneconnolly.testaylien.model.responses.stories;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "dbpedia"
})
public class Links {

    @JsonProperty("dbpedia")
    private String dbpedia;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public Links() {
    }

    /**
     * 
     * @param dbpedia
     */
    public Links(String dbpedia) {
        super();
        this.dbpedia = dbpedia;
    }

    @JsonProperty("dbpedia")
    public String getDbpedia() {
        return dbpedia;
    }

    @JsonProperty("dbpedia")
    public void setDbpedia(String dbpedia) {
        this.dbpedia = dbpedia;
    }

    public Links withDbpedia(String dbpedia) {
        this.dbpedia = dbpedia;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public Links withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("dbpedia", dbpedia).append("additionalProperties", additionalProperties).toString();
    }

}
