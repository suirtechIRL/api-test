package io.shaneconnolly.testaylien.model.responses.stories;

import lombok.Data;

import java.util.ArrayList;

@Data
public class StoriesResponse {

    private ArrayList<Story> stories;

    public Story getFirstStory(){
        return this.stories.get(0);
    }

}
