
package io.shaneconnolly.testaylien.model.responses.clusters;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "time",
    "story_count",
    "earliest_story",
    "latest_story",
    "representative_story",
    "location"
})
public class Cluster {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("time")
    private String time;
    @JsonProperty("story_count")
    private Integer storyCount;
    @JsonProperty("earliest_story")
    private String earliestStory;
    @JsonProperty("latest_story")
    private String latestStory;
    @JsonProperty("representative_story")
    private RepresentativeStory representativeStory;
    @JsonProperty("location")
    private Location location;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public Cluster() {
    }

    /**
     * 
     * @param earliestStory
     * @param latestStory
     * @param representativeStory
     * @param location
     * @param id
     * @param time
     * @param storyCount
     */
    public Cluster(Integer id, String time, Integer storyCount, String earliestStory, String latestStory, RepresentativeStory representativeStory, Location location) {
        super();
        this.id = id;
        this.time = time;
        this.storyCount = storyCount;
        this.earliestStory = earliestStory;
        this.latestStory = latestStory;
        this.representativeStory = representativeStory;
        this.location = location;
    }

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    public Cluster withId(Integer id) {
        this.id = id;
        return this;
    }

    @JsonProperty("time")
    public String getTime() {
        return time;
    }

    @JsonProperty("time")
    public void setTime(String time) {
        this.time = time;
    }

    public Cluster withTime(String time) {
        this.time = time;
        return this;
    }

    @JsonProperty("story_count")
    public Integer getStoryCount() {
        return storyCount;
    }

    @JsonProperty("story_count")
    public void setStoryCount(Integer storyCount) {
        this.storyCount = storyCount;
    }

    public Cluster withStoryCount(Integer storyCount) {
        this.storyCount = storyCount;
        return this;
    }

    @JsonProperty("earliest_story")
    public String getEarliestStory() {
        return earliestStory;
    }

    @JsonProperty("earliest_story")
    public void setEarliestStory(String earliestStory) {
        this.earliestStory = earliestStory;
    }

    public Cluster withEarliestStory(String earliestStory) {
        this.earliestStory = earliestStory;
        return this;
    }

    @JsonProperty("latest_story")
    public String getLatestStory() {
        return latestStory;
    }

    @JsonProperty("latest_story")
    public void setLatestStory(String latestStory) {
        this.latestStory = latestStory;
    }

    public Cluster withLatestStory(String latestStory) {
        this.latestStory = latestStory;
        return this;
    }

    @JsonProperty("representative_story")
    public RepresentativeStory getRepresentativeStory() {
        return representativeStory;
    }

    @JsonProperty("representative_story")
    public void setRepresentativeStory(RepresentativeStory representativeStory) {
        this.representativeStory = representativeStory;
    }

    public Cluster withRepresentativeStory(RepresentativeStory representativeStory) {
        this.representativeStory = representativeStory;
        return this;
    }

    @JsonProperty("location")
    public Location getLocation() {
        return location;
    }

    @JsonProperty("location")
    public void setLocation(Location location) {
        this.location = location;
    }

    public Cluster withLocation(Location location) {
        this.location = location;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public Cluster withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("time", time).append("storyCount", storyCount).append("earliestStory", earliestStory).append("latestStory", latestStory).append("representativeStory", representativeStory).append("location", location).append("additionalProperties", additionalProperties).toString();
    }

}
