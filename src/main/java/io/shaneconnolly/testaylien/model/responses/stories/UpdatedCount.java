package io.shaneconnolly.testaylien.model.responses.stories;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Date;

@Data
public class UpdatedCount {

    @JsonProperty("count")
    private int count;
    @JsonProperty("fetched_at")
    private Date fetchedAt;
}
