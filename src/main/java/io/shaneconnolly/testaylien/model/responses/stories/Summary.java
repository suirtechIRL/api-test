
package io.shaneconnolly.testaylien.model.responses.stories;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "sentences"
})
public class Summary {

    @JsonProperty("sentences")
    private List<String> sentences = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public Summary() {
    }

    /**
     * 
     * @param sentences
     */
    public Summary(List<String> sentences) {
        super();
        this.sentences = sentences;
    }

    @JsonProperty("sentences")
    public List<String> getSentences() {
        return sentences;
    }

    @JsonProperty("sentences")
    public void setSentences(List<String> sentences) {
        this.sentences = sentences;
    }

    public Summary withSentences(List<String> sentences) {
        this.sentences = sentences;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public Summary withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("sentences", sentences).append("additionalProperties", additionalProperties).toString();
    }

}
