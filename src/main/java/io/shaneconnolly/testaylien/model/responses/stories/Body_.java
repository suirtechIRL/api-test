
package io.shaneconnolly.testaylien.model.responses.stories;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "polarity",
    "score"
})
public class Body_ {

    @JsonProperty("polarity")
    private String polarity;
    @JsonProperty("score")
    private double score;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public Body_() {
    }

    /**
     * 
     * @param score
     * @param polarity
     */
    public Body_(String polarity, double score) {
        super();
        this.polarity = polarity;
        this.score = score;
    }

    @JsonProperty("polarity")
    public String getPolarity() {
        return polarity;
    }

    @JsonProperty("polarity")
    public void setPolarity(String polarity) {
        this.polarity = polarity;
    }

    public Body_ withPolarity(String polarity) {
        this.polarity = polarity;
        return this;
    }

    @JsonProperty("score")
    public double getScore() {
        return score;
    }

    @JsonProperty("score")
    public void setScore(double score) {
        this.score = score;
    }

    public Body_ withScore(double score) {
        this.score = score;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public Body_ withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("polarity", polarity).append("score", score).append("additionalProperties", additionalProperties).toString();
    }

}
