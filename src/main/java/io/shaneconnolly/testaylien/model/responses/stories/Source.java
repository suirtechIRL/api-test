
package io.shaneconnolly.testaylien.model.responses.stories;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "name",
    "home_page_url",
    "domain",
    "logo_url",
    "locations",
    "scopes",
    "rankings"
})
public class Source {

    @JsonProperty("id")
    private long id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("home_page_url")
    private String homePageUrl;
    @JsonProperty("domain")
    private String domain;
    @JsonProperty("logo_url")
    private String logoUrl;
    @JsonProperty("locations")
    private List<Location> locations = null;
    @JsonProperty("scopes")
    private List<Object> scopes = null;
    @JsonProperty("rankings")
    private Rankings rankings;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public Source() {
    }

    /**
     * 
     * @param rankings
     * @param domain
     * @param name
     * @param locations
     * @param id
     * @param scopes
     * @param homePageUrl
     * @param logoUrl
     */
    public Source(long id, String name, String homePageUrl, String domain, String logoUrl, List<Location> locations, List<Object> scopes, Rankings rankings) {
        super();
        this.id = id;
        this.name = name;
        this.homePageUrl = homePageUrl;
        this.domain = domain;
        this.logoUrl = logoUrl;
        this.locations = locations;
        this.scopes = scopes;
        this.rankings = rankings;
    }

    @JsonProperty("id")
    public long getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(long id) {
        this.id = id;
    }

    public Source withId(long id) {
        this.id = id;
        return this;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    public Source withName(String name) {
        this.name = name;
        return this;
    }

    @JsonProperty("home_page_url")
    public String getHomePageUrl() {
        return homePageUrl;
    }

    @JsonProperty("home_page_url")
    public void setHomePageUrl(String homePageUrl) {
        this.homePageUrl = homePageUrl;
    }

    public Source withHomePageUrl(String homePageUrl) {
        this.homePageUrl = homePageUrl;
        return this;
    }

    @JsonProperty("domain")
    public String getDomain() {
        return domain;
    }

    @JsonProperty("domain")
    public void setDomain(String domain) {
        this.domain = domain;
    }

    public Source withDomain(String domain) {
        this.domain = domain;
        return this;
    }

    @JsonProperty("logo_url")
    public String getLogoUrl() {
        return logoUrl;
    }

    @JsonProperty("logo_url")
    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    public Source withLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
        return this;
    }

    @JsonProperty("locations")
    public List<Location> getLocations() {
        return locations;
    }

    @JsonProperty("locations")
    public void setLocations(List<Location> locations) {
        this.locations = locations;
    }

    public Source withLocations(List<Location> locations) {
        this.locations = locations;
        return this;
    }

    @JsonProperty("scopes")
    public List<Object> getScopes() {
        return scopes;
    }

    @JsonProperty("scopes")
    public void setScopes(List<Object> scopes) {
        this.scopes = scopes;
    }

    public Source withScopes(List<Object> scopes) {
        this.scopes = scopes;
        return this;
    }

    @JsonProperty("rankings")
    public Rankings getRankings() {
        return rankings;
    }

    @JsonProperty("rankings")
    public void setRankings(Rankings rankings) {
        this.rankings = rankings;
    }

    public Source withRankings(Rankings rankings) {
        this.rankings = rankings;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public Source withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("name", name).append("homePageUrl", homePageUrl).append("domain", domain).append("logoUrl", logoUrl).append("locations", locations).append("scopes", scopes).append("rankings", rankings).append("additionalProperties", additionalProperties).toString();
    }

}
