
package io.shaneconnolly.testaylien.model.responses.stories;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "alexa"
})
public class Rankings {

    @JsonProperty("alexa")
    private List<Alexa> alexa = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public Rankings() {
    }

    /**
     * 
     * @param alexa
     */
    public Rankings(List<Alexa> alexa) {
        super();
        this.alexa = alexa;
    }

    @JsonProperty("alexa")
    public List<Alexa> getAlexa() {
        return alexa;
    }

    @JsonProperty("alexa")
    public void setAlexa(List<Alexa> alexa) {
        this.alexa = alexa;
    }

    public Rankings withAlexa(List<Alexa> alexa) {
        this.alexa = alexa;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public Rankings withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("alexa", alexa).append("additionalProperties", additionalProperties).toString();
    }

}
