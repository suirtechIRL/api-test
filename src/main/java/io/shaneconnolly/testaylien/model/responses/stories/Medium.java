
package io.shaneconnolly.testaylien.model.responses.stories;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "type",
    "url",
    "format",
    "content_length",
    "width",
    "height"
})
public class Medium {

    @JsonProperty("type")
    private String type;
    @JsonProperty("url")
    private String url;
    @JsonProperty("format")
    private String format;
    @JsonProperty("content_length")
    private long contentLength;
    @JsonProperty("width")
    private long width;
    @JsonProperty("height")
    private long height;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public Medium() {
    }

    /**
     * 
     * @param format
     * @param width
     * @param contentLength
     * @param type
     * @param url
     * @param height
     */
    public Medium(String type, String url, String format, long contentLength, long width, long height) {
        super();
        this.type = type;
        this.url = url;
        this.format = format;
        this.contentLength = contentLength;
        this.width = width;
        this.height = height;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    public Medium withType(String type) {
        this.type = type;
        return this;
    }

    @JsonProperty("url")
    public String getUrl() {
        return url;
    }

    @JsonProperty("url")
    public void setUrl(String url) {
        this.url = url;
    }

    public Medium withUrl(String url) {
        this.url = url;
        return this;
    }

    @JsonProperty("format")
    public String getFormat() {
        return format;
    }

    @JsonProperty("format")
    public void setFormat(String format) {
        this.format = format;
    }

    public Medium withFormat(String format) {
        this.format = format;
        return this;
    }

    @JsonProperty("content_length")
    public long getContentLength() {
        return contentLength;
    }

    @JsonProperty("content_length")
    public void setContentLength(long contentLength) {
        this.contentLength = contentLength;
    }

    public Medium withContentLength(long contentLength) {
        this.contentLength = contentLength;
        return this;
    }

    @JsonProperty("width")
    public long getWidth() {
        return width;
    }

    @JsonProperty("width")
    public void setWidth(long width) {
        this.width = width;
    }

    public Medium withWidth(long width) {
        this.width = width;
        return this;
    }

    @JsonProperty("height")
    public long getHeight() {
        return height;
    }

    @JsonProperty("height")
    public void setHeight(long height) {
        this.height = height;
    }

    public Medium withHeight(long height) {
        this.height = height;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public Medium withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("type", type).append("url", url).append("format", format).append("contentLength", contentLength).append("width", width).append("height", height).append("additionalProperties", additionalProperties).toString();
    }

}
