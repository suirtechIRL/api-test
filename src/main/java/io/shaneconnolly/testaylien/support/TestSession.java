package io.shaneconnolly.testaylien.support;

import cucumber.api.Scenario;
import io.shaneconnolly.testaylien.model.responses.clusters.ClustersResponse;
import io.shaneconnolly.testaylien.model.responses.errors.ErrorResponse;
import io.shaneconnolly.testaylien.model.responses.stories.StoriesResponse;
import io.shaneconnolly.testaylien.model.responses.stories.Story;
import lombok.Data;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
@Data
public class TestSession {

    private Scenario scenario;

    private ResponseEntity<StoriesResponse> storiesResponseResponseEntity;

    private ErrorResponse errorResponse;

    private ResponseEntity<ClustersResponse> clustersResponseResponseEntity;

    private Story lastSavedStory;


}