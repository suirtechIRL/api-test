package io.shaneconnolly.testaylien.support;

import io.qameta.allure.Step;
import org.junit.Assert;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Assertion wrapper to write to html reports / results store and lock down assertion methods
 *
 */

@Component
public class CustomAssertions {

    public void assertListNotEmpty(List list){
        logAssertion(String.format("assertListNotEmpty actual list size :%s ", list.size()));
        Assert.assertNotEquals(0, list.size());
    }

    public void assertGreaterThanOrEqualTo(int expected, int actual) {
        logAssertion(String.format("assertGreaterThanOrEqualTo expected:%s >= actual:%s", expected, actual));
        Assert.assertTrue(expected >= actual);
    }

    public void assertEquals(Object expected, Object actual) {
        logAssertion(String.format("assertEquals expected:%s == actual:%s", expected, actual));
        Assert.assertEquals(expected, actual);
    }

    public void assertLessThanOrEqualTo(int expected, int actual) {
        logAssertion(String.format("assertLessThanOrEqualTo expected:%s <= actual:%s", expected, actual));
        Assert.assertTrue(expected <= actual);
    }


    @Step
    public void logAssertion(String message){

    }
}
