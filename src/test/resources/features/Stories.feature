Feature: Stories

  Background: As An Api Consumer I want to query the Stories Endpoint given certain criteria


  @Smoke
  Scenario: Are Fresh Stories Available
    Given I search for New Stories
    Then I receive a set of New Stories


  @Regression @WIP
  Scenario: Are New Stories Volumes Comparable with Past Volumes
    Given I search for New Stories
    When I receive a set of New Stories
    Then the New Stories Volumes are Comparable with Past Volumes


