Feature: 400s Error Messages

  Background: As an API Consumer I want to understand clear http 40X messages


  @Smoke
  Scenario Outline: 401 Error Message
    Given I search for New Stories with no Auth Headers
    Then I receive a set 401 error with detail "<ErrorDetail>" and code "<ErrorCode>"

    Examples:
      | ErrorDetail                                                    | ErrorCode |
      | `X-Application-Id` or `X-Application-Key` headers are missing. | KB401     |

