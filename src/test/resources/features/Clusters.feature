Feature: Clusters

  Background: As An Api Consumer I want to query the Clusters Endpoint given certain criteria


  @Smoke
  Scenario Outline: Search Clusters by Story limits
    Given I search for New Clusters with "<story_count.min>" and "<story_count.max>" stories sizes
    Then I receive a set of Clusters with stories of "<story_count.min>" and "<story_count.max>"

    Examples:
      | story_count.min | story_count.max |
      | 10              | 20              |
      | 10000           | 10100           |

