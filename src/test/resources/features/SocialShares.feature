@Stories
Feature: Social Shares

  Background: As An Api Consumer I want to consume Social Sharing Data to predict interest


  @Smoke @SaveStories
  Scenario Outline: Social Shares updating
    Given I have a saved Story with id "<id>"
    When I retrieve the Story with id "<id>"
    Then I verify Social Share is updating on Story with id "<id>"

    Examples:
      | id       |
      | 73840434 |
