package io.shaneconnolly.testaylien.test.runner;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "classpath:features/http/400s.feature",
        glue = {
                "io.shaneconnolly.testaylien.test.steps",
        },
        plugin = {
                "pretty",
                "json:target/cucumber_jsons/400s.json"
        },
        monochrome = true)
public class Error404RunnerTest {


}
