package io.shaneconnolly.testaylien.test.runner;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "classpath:features/Clusters.feature",
        glue = {
                "io.shaneconnolly.testaylien.test.steps",
        },
        plugin = {
                "pretty",
                "json:target/cucumber_jsons/clusters.json"
        },
        monochrome = true)
public class ClustersRunnerTest {


}
