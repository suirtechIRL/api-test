package io.shaneconnolly.testaylien.test;

import io.shaneconnolly.testaylien.config.ClientConfig;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

@Configuration
@TestExecutionListeners(listeners = {DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class, TransactionalTestExecutionListener.class})
@ComponentScan({
        "io.shaneconnolly.testaylien"
})

@Import({
        ClientConfig.class
})
@PropertySource("classpath:/${testConfig}.properties")
public class TestConfig {

}