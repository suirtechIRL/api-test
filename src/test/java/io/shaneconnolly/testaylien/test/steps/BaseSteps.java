package io.shaneconnolly.testaylien.test.steps;

import io.shaneconnolly.testaylien.model.responses.stories.StoriesResponse;
import io.shaneconnolly.testaylien.model.responses.stories.Story;
import org.springframework.http.ResponseEntity;

public class BaseSteps {

    protected Story getFirstStory(ResponseEntity<StoriesResponse> responseEntity){
        return responseEntity.getBody().getStories().get(0);
    }

}
