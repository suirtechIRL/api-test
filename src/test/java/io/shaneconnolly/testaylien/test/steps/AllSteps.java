package io.shaneconnolly.testaylien.test.steps;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.shaneconnolly.testaylien.business.actions.VerifyResponseActions;
import io.shaneconnolly.testaylien.client.AylienClientImpl;
import io.shaneconnolly.testaylien.helpers.StoredDataHelper;
import io.shaneconnolly.testaylien.model.responses.errors.Error;
import io.shaneconnolly.testaylien.model.responses.stories.StoriesResponse;
import io.shaneconnolly.testaylien.model.responses.stories.Story;
import io.shaneconnolly.testaylien.support.CustomAssertions;
import io.shaneconnolly.testaylien.support.TestSession;
import io.shaneconnolly.testaylien.test.TestConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;

/**
 * steps would be broken up to logical class steps
 */

@ContextConfiguration(classes = TestConfig.class)
public class AllSteps extends BaseSteps {


    @Autowired
    private AylienClientImpl aylienClient;

    @Autowired
    private TestSession session;

    @Autowired
    private CustomAssertions customAssertions;

    @Autowired
    private StoredDataHelper storedDataHelper;

    @Autowired
    private VerifyResponseActions verifyResponseActions;

    @Given("^I search for New Stories$")
    public void i_search_for_New_Stories() throws Throwable {
        session.setStoriesResponseResponseEntity(aylienClient.getStories());
    }

    @Then("^I receive a set of New Stories$")
    public void i_receive_a_set_of_New_Stories() throws Throwable {
        ResponseEntity<StoriesResponse> storiesResponseEntity = session.getStoriesResponseResponseEntity();
        customAssertions.assertListNotEmpty(storiesResponseEntity.getBody().getStories());
    }

    @Then("^the New Stories Volumes are Comparable with Past Volumes$")
    public void the_New_Stories_Volumes_are_Comparable_with_Past_Volumes() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }


    @Given("^I have a saved Story with id \"([^\"]*)\"$")
    public void i_have_a_saved_Story_with_id(int id) throws Throwable {
        session.setLastSavedStory(storedDataHelper.getSavedStory(id));
    }

    @When("^I retrieve the Story with id \"([^\"]*)\"$")
    public void i_retrieve_the_Story_with_id(int id) throws Throwable {
        session.setStoriesResponseResponseEntity(aylienClient.getStory(id));
    }

    @Then("^I verify Social Share is updating on Story with id \"([^\"]*)\"$")
    public void i_verify_Social_Share_is_updating_on_Story_with_id(int id) throws Throwable {
        // steps so be simple and use actions to do complex business logic
        Story retrievedStory = getFirstStory(session.getStoriesResponseResponseEntity());
        Story savedStory = session.getLastSavedStory();

        verifyResponseActions.verifySocialSharesUpdating(
                session.getLastSavedStory(),
                savedStory
        );
        verifyResponseActions.verifyStoryId(id, retrievedStory);
        verifyResponseActions.verifyStoryId(id, savedStory);
    }


    @Given("^I search for New Clusters with \"([^\"]*)\" and \"([^\"]*)\" stories sizes$")
    public void i_search_for_New_Clusters_with_and_stories_sizes(int minStories, int maxStories) throws Throwable {
        session.setClustersResponseResponseEntity(aylienClient.getClusters(minStories, maxStories));
    }

    @Then("^I receive a set of Clusters with stories of \"([^\"]*)\" and \"([^\"]*)\"$")
    public void i_receive_a_set_of_Clusters_with_stories_of_and(int minStories, int maxStories) throws Throwable {
        verifyResponseActions.verifyMinMaxClusterStorySize(
                session.getClustersResponseResponseEntity().getBody().getClusters(),
                minStories,
                maxStories
        );
    }

    @Given("^I search for New Stories with no Auth Headers$")
    public void i_search_for_New_Stories_with_no_Auth_Headers() throws Throwable {
        session.setErrorResponse(aylienClient.getStoriesNoAuth());
    }

    @Then("^I receive a set (\\d+) error with message \"([^\"]*)\"$")
    public void i_receive_a_set_error_with_message(int arg1, String arg2) throws Throwable {

    }

    @Then("^I receive a set 401 error with detail \"([^\"]*)\" and code \"([^\"]*)\"$")
    public void i_receive_a_set_error_with_detail_and_code(String detail, String apiErrorCode) throws Throwable {
        Error authError = session.getErrorResponse().getErrors().get(0);
        customAssertions.assertEquals(detail, authError.getDetail());
        customAssertions.assertEquals(apiErrorCode, authError.getCode());
    }


}
