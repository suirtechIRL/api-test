package io.shaneconnolly.testaylien.test.steps;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import io.shaneconnolly.testaylien.helpers.StoredDataHelper;
import io.shaneconnolly.testaylien.support.TestSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;

public class Hooks {

    private static Logger logger = LogManager.getLogger(Hooks.class);

    @Autowired
    private TestSession session;

    @Autowired
    private StoredDataHelper storedDataHelper;

    @Before
    public void beforeScenario(Scenario scenario) {
        logger.info("*********************************************");
        logger.info("BEFORE SCENARIO ::: " + scenario.getName());
        logger.info("*********************************************");
        session.setScenario(scenario);
    }

    @After
    public void afterScenario() throws IOException {
        logger.info("*********************************************");
        logger.info("After SCENARIO ::: " + session.getScenario().getName() + " : " + session.getScenario().getStatus());
        logger.info("*********************************************");

    }


    @After("@SaveStories")
    public void afterScenarioSaveStores() throws IOException {
        saveStoriesResponseBody();
    }


    private void saveStoriesResponseBody() throws IOException {
        storedDataHelper.saveStory(session.getStoriesResponseResponseEntity().getBody().getStories().get(0));
    }
}
