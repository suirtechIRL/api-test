#!/usr/bin/env bash

mvn -U -DtestConfig=live clean test

cp ./src/main/resources/live.properties target/allure-results/environment.properties


echo "

    tests complete,

    if allure is installed:
        run: allure serve target/allure-results
    if not:
        goto: http://allure.suirtech.com:8601
        for a saved report


"